PT. XYZ adalah sebuah perusahaan fintech yang ingin mengembangkan mobile apps mereka, dalam upaya

menjangkau pengguna yang lebih luas mereka ingin mengembakan aplikasi pinjaman online.

1. Deskripsi gambar :

![My Image](flow.jpg)


Dari gambar diatas kita lihat SOA consume data dari core dan back end spring boot yang running di openshift melakukan consume data dari SOA.

pada pipeline CI/CD nya menggunakan jenkins.
Setiap aktifitas log yang di hasilkan dari backend springboot akan masuk ke dalam Kibana.

Selanjutnya, aktifitas peformance akan di handle oleh service worker dari request openAPI pada saat kondisi server tidak stabil dalam hal peformance maupun dalam keadaan normal.

2. Spesification api yg relate untuk menghandle :

**<h2>API Pengajuan pinjaman</h2>**

| API title | account/pengajuanPinjaman |
| --- | --- |
| API Version | v0.1 |
| API URL | api.pinjol/pinjaman |

**<h3>Account Inquiry</h3>**

| HTTP Method | POST |
| --- | --- |
| Path | api/v01/accountInquiry |
| Format Type | JSON |
| Authentication | JWT Token |

**<h4>Header Structure</h4>**

| **Key** | **Value** | **Format** | **Mandatory** | **Length** | **Description** |
| --- | --- | --- | --- | --- | --- |
| Authorization | Authorization | String | M   |     | Token Bearer from JWT |
| X-CLIENT-ID |     | String | M   | 32  | ID login peminjam |
| x-timestamp | timestamp | Datetime | M   |     |     |
| content-type | application/ json |     | M   |     |     |

**<h4>Request structure</h4>**

| **Field** | **Data Type** | **format** | **mandatory** | **length** | **deskripsi** | **contoh** |
| --- | --- | --- | --- | --- | --- | --- |
| accountNo | String | numeric | M   | 13  | Nomor KTP | 1212123123134 |
| DeviceID | string |     | M   | 8   | Device login type | mobile |

**<h4>Response structure</h4>**

| **Field** | **Data Type** | **format** | **mandatory** | **length** | **deskripsi** | **contoh** |
| --- | --- | --- | --- | --- | --- | --- |
| clientNo | String | numeric | M   | 13  | Nomor KTP | 1212123123134 |
| accountNo | String | numeric | M   | 20  | Nomor rekening | 55234249 |
| accountName | String | Alphanumeric | M   | 50  | Nama peminjam | Budi irawan |
| accountAddress | string | alphanumeric | M   | 100 | Alamat peminjam | Daan mogot |
| accountBirthdate | Date | date | M   | 10  | Tanggal lahir peminjam | 01-01-2001 |
| bankName | String | alpganumeric | M   | 35  | Nama bank | Bank toib |
| BankCode | String | numeric | M   | 3   | Kode Bank | 123 |
| limit | String | numeric | M   | 85  | Total limit | 10000000 |
| responseMessage | String | alphanumeric | M   | 80  | Respon message request | Inquiry succeessfully |
| responseCode | String | numeric | M   | 6   | Response code | 200 |
| DeviceID | string |     | M   | 8   | Device login type | mobile |

**<h4>Sample</h4>**

**<h5>Header Request</h5>**

curl curl --location --request POST '<https://api.pinjol/pinjaman/api/v01/accountInquiry?clientNo=1212123123134&DeviceID=mobile> \\ --header 'X-Timestamp: 2024-02-23T00:01:01.024Z' \\--header 'X-CLIENT-ID: 2112d12dqawe1e12=' \\ --header 'Authorization: Bearer 121312xsxssawq3‘'

**Body Request**
```json
    { "accountNo": "1212123123134",
       "DeviceID": "mobile"}
```


**Normal Response**

```json
{ "clientNo": "1212123123134",
"accountNo": "55234249",
"accountName": "Budi irawan",
"accountAddress": "Daan mogot",
"accountBirthdate": "01-01-2001",
"bankName": “bank toib",
"bankCode": “123",
"limit": “10000000",
"responseMessage": "inquiry succeessfully",
"responseCode": "200",
"DeviceID": "mobile"}
```

**Error response**

```json
{ "accountNo": "1212123123134",
"responseMessage": "clientNo must numeric",
"responseCode": "401",
"DeviceID": "mobile"}
```

**<h4>Mapping Response Code</h4>**

| **Response Code** | **Description** |
| --- | --- |
| 200 | Sukses |
| 500 | Gagal |
| 401 | Invalid |

**<h3>Pengajuan Pinjaman</h3>**

| HTTP Method | POST |
| --- | --- |
| Path | api/v01/pengajuanPinjaman |
| Format Type | JSON |
| Authentication | JWT Token |

**<h4>Header Structure</h4>**

| **Key** | **Value** | **Format** | **Mandatory** | **Length** | **Description** |
| --- | --- | --- | --- | --- | --- |
| Authorization | Authorization | String | M   |     | Token Bearer from JWT |
| X-CLIENT-ID |     | String | M   | 32  | ID login peminjam |
| x-timestamp | timestamp | Datetime | M   |     |     |
| content-type | application/ json |     | M   |     |     |

**<h4>Request structure</h4>**

| **Field** | **Data Type** | **format** | **mandatory** | **length** | **deskripsi** | **contoh** |
| --- | --- | --- | --- | --- | --- | --- |
| clientNo | String | numeric | M   | 13  | Nomor KTP | 1212123123134 |
| accountNo | String | numeric | M   | 15  | Nomor rekening | 55234249 |
| bankCode | String | numeric | M   | 3   | Kode Bank | 123 |
| tenor | String | numeric | M   | 5   | Tenor pinjaman dalam hitungan bulan | 24  |
| nominal | String | numeric | M   | 85  | Nominal pinjaman | 5000000 |
| DeviceID | string |     | M   | 8   | Device login type | mobile |

**<h4>Response structure</h4>**

| **Field** | **Data Type** | **format** | **mandatory** | **length** | **deskripsi** | **contoh** |
| --- | --- | --- | --- | --- | --- | --- |
| clientNo | String | numeric | M   | 13  | Nomor KTP | 1212123123134 |
| contractNo | String | numeric | M   | 25  | Nomor kontrak pinjaman | 1234561222 |
| responseMessage | String | alphanumeric | M   | 80  | Respon message request | Pinjaman succeessfully |
| responseCode | String | numeric | M   | 6   | Response code | 200 |
| DeviceID | string |     | M   | 8   | Device login type | mobile |

**<h4>Sample</h4>**

**Header Request**

curl curl --location --request POST '<https://api.pinjol/accountInquiry/pengajuanPinjaman?clientNo=1212123123134&accountNo=1212123123134&bankCode=123&tenor=24&nominal=5000000&DeviceID=mobile> \\ --header 'X-Timestamp: 2024-02-23T00:01:01.024Z' \\--header 'X-CLIENT-ID: 2112d12dqawe1e12=' \\ --header 'Authorization: Bearer 121312xsxssawq3‘'

**Body Request**

```json
{ "clientNo": "1212123123134",
"accountNo": "55234249",
"bankCode": "123",
"tenor": "24",
"nominal": "5000000",
"DeviceID": "mobile"}
```


**Normal Response**

```json
{ "clientNo": "1212123123134",
"contractNo": "1234561222",
"responseMessage": "pinjaman succeessfully",
"responseCode": "200",
"DeviceID": "mobile"}
```

**Error response**
```json
{ "clientNo": "1212123123134",
"responseMessage": "bankCode must numeric",
"responseCode": "401",
"DeviceID": "mobile"}
```

**<h4>Mapping Response Code</h4>**

| **Response Code** | **Description** |
| --- | --- |
| 200 | Sukses |
| 500 | Gagal |
| 401 | Invalid |

**<h3>Contract Inquiry</h3>**

| HTTP Method | POST |
| --- | --- |
| Path | api/v01/contractInquiry |
| Format Type | JSON |
| Authentication | JWT Token |

**<h4>Header Structure</h4>**

| **Key** | **Value** | **Format** | **Mandatory** | **Length** | **Description** |
| --- | --- | --- | --- | --- | --- |
| Authorization | Authorization | String | M   |     | Token Bearer from JWT |
| X-CLIENT-ID |     | String | M   | 32  | ID login peminjam |
| x-timestamp | timestamp | Datetime | M   |     |     |
| content-type | application/ json |     | M   |     |     |

**<h4>Request structure</h4>**

| **Field** | **Data Type** | **format** | **mandatory** | **length** | **deskripsi** | **contoh** |
| --- | --- | --- | --- | --- | --- | --- |
| contractNo | String | numeric | M   | 25  | Nomor kontrak | 1234561222 |
| DeviceID | string |     | M   | 8   | Device login type | mobile |

**<h4>Response structure</h4>**

| **Field** | **Data Type** | **format** | **mandatory** | **length** | **deskripsi** | **contoh** |
| --- | --- | --- | --- | --- | --- | --- |
| clientNo | String | numeric | M   | 13  | Nomor KTP | 1212123123134 |
| contractNo | String | numeric | M   | 25  | Nomor kontrak | 1234561222 |
| accountNo | String | numeric | M   | 20  | Nomor rekening | 55234249 |
| accountName | String | Alphanumeric | M   | 50  | Nama peminjam | Budi irawan |
| accountAddress | string | alphanumeric | M   | 100 | Alamat peminjam | Daan mogot |
| accountBirthdate | Date | date | M   | 10  | Tanggal lahir peminjam | 01-01-2001 |
| bankName | String | alpganumeric | M   | 35  | Nama bank | Bank toib |
| BankCode | String | numeric | M   | 3   | Kode Bank | 123 |
| limit | String | numeric | M   | 85  | Total limit | 10000000 |
| cicilanNo | String | numeric | M   | 5   | Total angsuran | 5   |
| totalTunggakanNo | String | numeric | M   | 5   | Total tunggakan pembayaran cicilan | 1   |
| tunggakanNo | String | numeric | M   | 5   | Tunggakan dari pembayaran cicilan | 3   |
| responseMessage | String | alphanumeric | M   | 80  | Respon message request | Inquiry succeessfully |
| responseCode | String | numeric | M   | 6   | Response code | 200 |
| DeviceID | string |     | M   | 8   | Device login type | mobile |

**<h4>Sample</h4>**

**Header Request**

curl curl --location --request POST '<https://api.pinjol/pinjaman/api/v01/accountInquiry?contractNo=1234561222&DeviceID=mobile> \\ --header 'X-Timestamp: 2024-02-23T00:01:01.024Z' \\--header 'X-CLIENT-ID: 2112d12dqawe1e12=' \\ --header 'Authorization: Bearer 121312xsxssawq3‘'

**Body Request**

```json
{ "contractNo": "1234561222",
"DeviceID": "mobile"}
```

**Normal Response**

```json
{ "contractNo": "1234561222",
"clientNo": "1212123123134",
"accountNo": "55234249",
"accountName": "Budi irawan",
"accountAddress": "Daan mogot",
"accountBirthdate": "01-01-2001",
"bankName": “bank toib",
"bankCode": “123",
"limit": “10000000",
"cicilanNo": “5",
"totalTunggakanNo": “1",
"tunggakanNo": “3",
"responseMessage": "inquiry succeessfully",
"responseCode": "200",
"DeviceID": "mobile"}
```


**Error response**
```json
{ "contractNo": "1234561222",
"responseMessage": "contractNo must numeric",
"responseCode": "401",
"DeviceID": "mobile"}
```


**<h4>Mapping Response Code</h4>**

| **Response Code** | **Description** |
| --- | --- |
| 200 | Sukses |
| 500 | Gagal |
| 401 | Invalid |
